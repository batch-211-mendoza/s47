const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", displayFullName);
txtLastName.addEventListener("keyup", displayFullName);

function displayFullName(){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}